<?php
/*
./src/Controller/creaturesController.php

* Controller des créatures
*
* Action disponible show,index,add,edit,delete,search
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Creatures;
use App\Form\CreaturesType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller des créatures
 */
class CreaturesController extends GenericController {


    /**
     * Function qui affiche le détails d'une Creature
     * @param  int     $id      id de la Creatures à afficher
     * @param  Request $request OBJECT
     * @return Response Creatures
     */

    public function showAction(int $id, Request $request){
      $creature = $this->_repository->find($id);
      return $this->render('creatures/show.html.twig',[
        'creature'   => $creature,
      ]);
    }


    /**
     * Function qui affiche l'index des Creatures
     * @param  string $vue      contient la vue
     * @param  array  $orderBy  paramètre pour l'OrderBy
     * @param  int $limit    paramètre pour la Limit
     * @param  int $creature paramètre pour définir le Where
     * @return array
     */

    public function indexAction(string $vue , array $orderBy = ['datecreation' => 'DESC'] ,int $limit = null,int $creature = null){
      $where =($creature !== null)?['creature'=>$creature]:[];
      $creatures = $this->_repository->findBy($where, $orderBy, $limit);
      return $this->render('creatures/'.$vue.'.html.twig',[
      'creatures' => $creatures
        ]);
      }


      /**
       * Function qui rajoute une créature
       * @param Request $request OBJECT
       */
      public function addAction(Request $request) {
      $creature = new Creatures();
      $form = $this->createForm(CreaturesType::class, $creature,[
      'action'=>$this->generateUrl('app.creatures.add')
  ]);
      $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
          $manager = $this->getDoctrine()->getManager();
          $manager->persist($creature);
          $manager->flush();
          $this->get('session')->getFlashBag()->clear();
          $this->addFlash('message', "Your céature was add with success");
          return $this->redirectToRoute('app.creatures.show',['id'=> $creature->getId(),
          'slug'=> $creature->getSlug()]);
        }
        return $this->render('creatures/add.html.twig', [
        'form' => $form->createView(),
        'creature' => $creature
    ]);
}


     /**
      * Function qui Edit une Creature
      * @param  Request   $request  OBJECT
      * @param  Creatures $creature permet de faire une findOneById
      * @return array   contenant le formulaire et les infos de la créature que l'on edit
      */
    public function editAction(Request $request,Creatures $creature) {
    $form = $this->createForm(CreaturesType::class, $creature,[
    'action'=>$this->generateUrl('app.creatures.edit',array('id'=>$creature->getId(),'slug'=> $creature->getSlug()))
   ]);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {        $manager = $this->getDoctrine()->getManager();
      $manager->persist($creature);
      $manager->flush();
      $this->get('session')->getFlashBag()->clear();
      $this->addFlash('message', "Your creature was modify with success");
      return $this->redirectToRoute('app.creatures.show',['id'=> $creature->getId(),'slug'=> $creature->getSlug()]);
      }
      return $this->render('creatures/edit.html.twig', [
      'form' => $form->createView(),
      'creature' => $creature
  ]);
    }


    /**
     * Function qui Supprime une Creature
     * @param  Creatures $creature permet de faire une findOneById
     * @return void
     */
    public function deleteAction(Creatures $creature) {
    $manager = $this->getDoctrine()->getManager();
    $manager->remove($creature);
    $manager->flush();
    $this->get('session')->getFlashBag()->clear();
    $this->addFlash('message', "Your Créature was remove with success");
    return $this->redirectToRoute('app.homepage');
  }

  /**
   * Function qui permet la recherche de créatures
   * @param  Request $request OBJECT
   * @return array  contenant le résultat de la recherche
   */
  public function searchAction(Request $request){
    $search=$request->query->get('search');
    $creatures = $this->_repository->findSearch($search);
    return $this->render('creatures/search.html.twig',[
      'search' => $search,
      'creatures' => $creatures
    ]);
  }



}
