<?php

/*
./src/Form/CreatureType.php

* Form des Tags
*
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Form;

use App\Entity\Creatures;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Type de Creatures
 */
class CreaturesType extends AbstractType
{
    /**
     * function qui crée le formulaire
     * @param  FormBuilderInterface $builder [description]
     * @param  array                $options [description]
     * @return void
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('textelead')
            ->add('textesuite')
            ->add('image')
            ->add('slug')
            ->add('film',EntityType::class, [
                  'class' => \App\Entity\Films::class,
                  'choice_value' => 'id',
                  'choice_label' => 'titre'
            ])
            ->add('tag',EntityType::class, [
                  'class' => \App\Entity\Tags::class,
                  'choice_value' => 'id',
                  'choice_label' => 'nom',
                'multiple' => true,
                'expanded' => true
            ])
           ->add('save', Type\SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Creatures::class,
        ]);
    }
}
