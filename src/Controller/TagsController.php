<?php
/*
./src/Controller/TagsController.php

* Controller des Tags
*
* Action disponible show,index
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tags;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller des Tags
 */
class TagsController extends GenericController {
    /**
     * Function qui affiche le détails des Tags
     * @param  int     $id      id du Tags
     * @param  Request $request OBJECT
     * @return Reponse Tags contenant les infos du Tags
     */
    public function showAction(int $id, Request $request){
      $tag = $this->_repository->find($id);
      return $this->render('tags/show.html.twig',[
        'tag'   => $tag,

      ]);
    }
      /**
       * Function qui affiche l'index des Tags
       * @return Reponse Tags contenant les infos pour afficher l'index des Tags
       */
      public function indexAction(){
  $tags = $this->_repository->findAll();
  return $this->render('tags/index.html.twig',[
    'tags' => $tags
  ]);
}

}
