<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190322143824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE creatures CHANGE film film INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE creatures_has_tags RENAME INDEX fk_creatures_has_tags_creatures1_idx TO IDX_38BEBCA52A6C6AF4');
        $this->addSql('ALTER TABLE creatures_has_tags RENAME INDEX fk_creatures_has_tags_tags1_idx TO IDX_38BEBCA5389B783');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE creatures CHANGE film film INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE creatures_has_tags RENAME INDEX idx_38bebca5389b783 TO fk_creatures_has_tags_tags1_idx');
        $this->addSql('ALTER TABLE creatures_has_tags RENAME INDEX idx_38bebca52a6c6af4 TO fk_creatures_has_tags_creatures1_idx');
    }
}
