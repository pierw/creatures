<?php
/*
./src/Controller/FilmsController.php

* Controller des films
*
* Action disponible show,index
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Films;
use App\Entity\Creatures;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller des Films
 */
class FilmsController extends GenericController {
    /**
     * Function qui affiche le détails d'un Films
     * @param  int     $id     id du Films à aficher
     * @param  Request $request OBJECT
     * return Response Films contenant les infos du Films
     */
    public function showAction(int $id, Request $request){


      $creatures = $this->getDoctrine()
                       ->getRepository(\App\Entity\Creatures::class)
                       ->findByFilm($id);

      $film = $this->_repository->find($id);
      return $this->render('films/show.html.twig',[
        'film'   => $film,
        'creatures'=>$creatures
      ]);
    }
    /**
     * Function qui affiche l'index des Films
     * @return Response Films contenant les infos des Films à aficher
     */
      public function indexAction(){
  $films = $this->_repository->findAll();
  return $this->render('films/index.html.twig',[
    'films' => $films
  ]);
}

}
