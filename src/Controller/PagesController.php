<?php
/*
./src/Controller/PagesController.php

* Controller des Pages
*
* Action disponible show,index
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Pages;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller des Pages
 */
class PagesController extends GenericController {
    /**
     * Function qui affiche le détail d'une page
     * @param  int     $id      id de la page s
     * @param  Request $request OBJECT
     * @return Response Pages contenant les infos de la pages
     */
    public function showAction(int $id, Request $request){

      $page = $this->_repository->find($id);
      return $this->render('pages/show.html.twig',[
        'page'   => $page,

      ]);
    }
    /**
     * Function qui affiche l'index des pages
     * @param  string $test  paramètre pour le highlight
     * @param  string $where paramètre pour le Where
     * @param  int $limit paramètre pour le Limit
     * @return Response Pages contenant les infos des pages
     */
    public function indexAction($route,$id,$where=null,$limit=null){

        $pages = $this->_repository->findBy(
                                  [],
                                  ['tri' => 'asc'],
                                  $limit
                                );
        return $this->render('pages/index.html.twig',[
        'pages' => $pages,
        'route' => $route,
        'id' => $id
      ]);
    }

}
