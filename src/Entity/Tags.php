<?php
/*
./src/Entity/Tags.php

* Entity des Tags
*
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tags
 *
 * @ORM\Table(name="tags")
 * @ORM\Entity
 *
 * Objet Tags
 */
class Tags
{
    /**
     *Id de Tags
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *Nom de Tags
     *
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=45, nullable=false)
     */
    private $nom;

    /**
     * Slug de Tags
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
     */
    private $slug;

    /**
     * creature de Pages
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Creatures", mappedBy="tag")
     */
    private $creature;

    /**
     * Constructor
     * Crée une collection de tableau de creature pour Tags
     */
    public function __construct()
    {
        $this->creature = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Récupère l'id de tags
     * @return int Tags
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Récupère le nom de Tags
     * @return string Tags
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }
    /**
     * Défini le nom de Tags
     * @param  string $nom
     * @return self  Tags
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    /**
     * Récupère le slug de Tags
     * @return string Tags
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * Défini le slug de Tags
     * @param  string $slug
     * @return self  Tags
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Récupère la collection decreature de Tags
     * @return Collection|Creatures[]
     */
    public function getCreature(): Collection
    {
        return $this->creature;
    }
    /**
     * Ajoute une creature à Tags
     * @param  Creatures $creature
     * @return self Tags
     */
    public function addCreature(Creatures $creature): self
    {
        if (!$this->creature->contains($creature)) {
            $this->creature[] = $creature;
            $creature->addTag($this);
        }

        return $this;
    }
    /**
     * Efface des creature de Tags
     * @param  Creatures $creature
     * @return self  Tags
     */
    public function removeCreature(Creatures $creature): self
    {
        if ($this->creature->contains($creature)) {
            $this->creature->removeElement($creature);
            $creature->removeTag($this);
        }

        return $this;
    }

}
