<?php
/*
./src/Repository/CreaturesRepository.php

* Repository de Creatures
*
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Repository;

use App\Entity\Creatures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

/**
 * class Repository de Creatures
 */
class CreaturesRepository extends ServiceEntityRepository
{

    /**
     * Constructeur du Repository
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Creatures::class);
    }

    // /**
    //  * @return Page[] Returns an array of Page objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Page
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * Function qui recherche par film $id
     * @param  int $id [description]
     * @return [type]     [description]
     */
    public function findByFilm($id){
        return $this->createQueryBuilder('c')
            ->andWhere('c.film = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

  /**
   *  Function moteur de recherche
   * @param  [type] $search mot-clé entrer par l'user
   * @return array object
   */
    public function findSearch($search){
      // $qb->addSelect('(MATCH (c.nom, c.textelead, c.textesuite, f.titre, tags.nom) AGAINST (:keyword )) AS HIDDEN mySelectAlias');
      // $qb->setParameter("keyword_", array_values($keyword));
      //
      // $group          = $entityManager->find('Group', $groupId);
      // $userCollection = $group->getUsers();
      //
      // $criteria = Criteria::create()
      //     ->where(Criteria::expr()->eq("birthday", "1982-02-17"))
      //     ->orderBy(array("username" => Criteria::ASC))
      //     ->setFirstResult(0)
      //     ->setMaxResults(20)
      // ;
      //
      // $birthdayUsers = $userCollection->matching($criteria);
      $word =[];
      $word = explode(' ', $search);
       $qb =$this->createQueryBuilder('c');
       // $qb->addSelect("(CASE WHEN name like 'John %' THEN 0
       //     WHEN name like 'John%' THEN 1
       //     WHEN name like '% John%' THEN 2
       //     ELSE 3 END) AS HIDDEN ORD ");
       $qb->join('c.film', 'f');
       $qb->innerjoin('c.tag', 'tags');
       foreach ($word as $i => $keyword) {
         $qb->Orwhere($qb->expr()->orX(
             $qb->expr()->like('c.nom', ":keyword_".$i),
             $qb->expr()->like('c.textelead', ":keyword_".$i),
             $qb->expr()->like('c.textesuite', ":keyword_".$i),
             $qb->expr()->like('f.titre', ":keyword_".$i),
             $qb->expr()->like('tags.nom', ":keyword_".$i)
         ));
         $qb->setParameter("keyword_".$i, '%'.$keyword.'%');
       }

       $qb->orderBy('c.nom', 'ASC');
      return  $qb->getQuery()->getResult();
}

}
