<?php
/*
./src/Entity/Creatures.php

* Entity des Creatures
*
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Creatures
 *
 * @ORM\Table(name="creatures", indexes={@ORM\Index(name="fk_creatures_films_idx", columns={"film"})})
 * @ORM\Entity
 *
 * Objet Creatures
 */

class Creatures
{


    /**
     *
     *
     * Id de Creatures
     * @var int
     * id de Creatures
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */

    private $id;

    /**
     * Nom de Creatures
     * @var string
     * nom de Creatures
     * @ORM\Column(name="nom", type="string", length=45, nullable=false)
     */
    private $nom;

    /**
     * texteLead de Creatures
     * @var string|null
     * texteLead de Creatrures
     * @ORM\Column(name="texteLead", type="text", length=65535, nullable=true)
     */
    private $textelead;

    /**
     * @var string|null
     * texteSuite de Creatures si pas null
     * @ORM\Column(name="texteSuite", type="text", length=65535, nullable=true)
     */
    private $textesuite;

    /**
     * datecreation de Creatures
     * @var \DateTime
     * date de création de Creatures
     * @ORM\Column(name="dateCreation", type="datetime", nullable=false)
     */
    private $datecreation;

    /**
     * image deCreatures
     * @var string|null
     * image de Creatures
     * @ORM\Column(name="image", type="string", length=45, nullable=true)
     */
    private $image;

    /**
     * slug de Creatures
     * @var string
     * slug de Creature
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
     */
    private $slug;

    /**
     * film de Creatures
     * @var \Films
     * film de Creature
     * @ORM\ManyToOne(targetEntity="Films")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="film", referencedColumnName="id")
     * })
     */
    private $film;

    /**
     * tag de Creatures
     * @var \Doctrine\Common\Collections\Collection
     * tag de Creature
     * @ORM\ManyToMany(targetEntity="Tags", inversedBy="creature")
     * @ORM\JoinTable(name="creatures_has_tags",
     *   joinColumns={
     *     @ORM\JoinColumn(name="creature", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tag", referencedColumnName="id")
     *   }
     * )
     */
    private $tag;

    /**
     * Constructor
     * Crée une collection de tableau de tag pour Creature
     */

    public function __construct()  {
      
        $this->setDateCreation(new \DateTime());
        $this->tag = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Récupère l'id de Creatures
     * @return int de Creatures
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Récupère le nom de Creatures
     * @return string de Creature
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * Défini le nom de l'Objet
     * @param  string $nom
     * @return self Creature
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Récupère le textelead de l'Objet
     * @return string de Creature
     */
    public function getTextelead(): ?string
    {
        return $this->textelead;
    }

    /**
     * Défini le textelead de l'Objet
     * @param  ?string $textelead
     * @return self  Creature
     */
    public function setTextelead(?string $textelead): self
    {
        $this->textelead = $textelead;

        return $this;
    }


    /**
     * Défini le texteSuite de Creature
     * @return string de Creature
     */
    public function getTextesuite(): ?string
    {
        return $this->textesuite;
    }

    /**
     * Défini le textesuite de Creature
     * @param  ?string $textesuite
     * @return self  Creature
     */
    public function setTextesuite(?string $textesuite): self
    {
        $this->textesuite = $textesuite;

        return $this;
    }

    /**
     * Récupère la dateCreation de Creature
     * @return datetime de Creature
     */
    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }


    /**
     * Défini la dateCreation de Creature
     * @param  DateTimeInterface $datecreation
     * @return self  Creatures
     */
    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }


    /**
     * Récupère l'image de Creature
     * @return string de Creature
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * Défini l'image de Creature
     * @param  ?string $image
     * @return self  Creature
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }


    /**
     * Récupère le slug de Creature
     * @return string slug de creature
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Défini le slug de Creature
     * @param  string $slug
     * @return self  Creature
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     *Récupère les films de Creatures
     * @return string film de Creatures
     */
    public function getFilm(): ?Films
    {
        return $this->film;
    }
    /**
     * Défini les Films de Creatures
     * @param  ?Films $film
     * @return self  Creatures
     */
    public function setFilm(?Films $film): self
    {
        $this->film = $film;

        return $this;
    }

    /**
     * @return Collection|Tags[]
     * Récupère la collection de tag de Creatures
     */

    public function getTag(): Collection
    {
        return $this->tag;
    }
    /**
     * Ajoute des tags à Creatures
     * @param  Tags $tag
     * @return self
     */
    public function addTag(Tags $tag): self
    {
        if (!$this->tag->contains($tag)) {
            $this->tag[] = $tag;
        }

        return $this;
    }
    /**
     * Efface des tags de Creatures
     * @param  Tags $tag
     * @return self
     */
    public function removeTag(Tags $tag): self
    {
        if ($this->tag->contains($tag)) {
            $this->tag->removeElement($tag);
        }

        return $this;
    }

}
