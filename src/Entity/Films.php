<?php
/*
./src/Entity/Films.php

* Entity des Films
*
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Films
 *
 * @ORM\Table(name="films")
 * @ORM\Entity
 *
 * Objet Films
 */
class Films
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=45, nullable=false)
     */
    private $titre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="synopsis", type="text", length=65535, nullable=true)
     */
    private $synopsis;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
     */
    private $slug;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="string", length=45, nullable=true)
     */
    private $image;
    /**
     * Récupère l'id de Films
     * @return int de Films
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Récupère le titre de Films
     * @return string de Films
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    /**
     * Défini le titre de Films
     * @param  string $titre de Films
     * @return self  Films
     */
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }
    /**
     * Récupère la synopsis de films
     * @return string de Films
     */
    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }
    /**
     * Défini la synopsis de films
     * @param  ?string $synopsis de films
     * @return self  Films
     */
    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }
    /**
     * Récupère le slug de Films
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * Défini le slug de films
     * @param  string $slug de Films
     * @return self  Films
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
    /**
     * Récupère l'image de Films
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }
    /**
     * Défini l'image de Films
     * @param  ?string $image de Films
     * @return self Films
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }


}
