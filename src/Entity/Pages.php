<?php

/*
./src/Entity/Pages.php

* Entity des Pages
*
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pages
 *
 * @ORM\Table(name="pages")
 * @ORM\Entity
 *
 * Objet Pages
 */
class Pages
{
    /**
     * Id Pages
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Titre Pages
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=45, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
     */
    private $slug;

    /**
     * texte Pages
     * @var string|null
     *
     * @ORM\Column(name="texte", type="text", length=65535, nullable=true)
     */
    private $texte;

    /**
     * tri Pages
     * @var int
     *
     * @ORM\Column(name="tri", type="integer", nullable=false)
     */
    private $tri;
    /**
     * Récupère l'id de Pages
     * @return [type] [description]
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Récupère le titre de Pages
     * @return [type] [description]
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    /**
     * Défini le titre de Pages
     * @param  string $titre
     * @return self Pages
     */
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }
    /**
     * Récupère le slug de Pages
     * @return string de Pages
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * Défini le slug de Pages
     * @param  string $slug
     * @return self Pages
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
    /**
     * Récupère le texte de Pages
     * @return string de Pages
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }
    /**
     * Défini le texte de Pages
     * @param  ?string $texte
     * @return self Pages
     */
    public function setTexte(?string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }
    /**
     * Récupère le champs tri de Pages
     * @return int de Pages
     */
    public function getTri(): ?int
    {
        return $this->tri;
    }
    /**
     * Défini le champs tri de Pages
     * @param  int  $tri 
     * @return self  Pages
     */
    public function setTri(int $tri): self
    {
        $this->tri = $tri;

        return $this;
    }


}
